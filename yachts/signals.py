from django.db.models.signals import pre_save, pre_delete
from django.dispatch import receiver

from .models import Yacht, Photo, Accommodation


@receiver(pre_save, sender=Yacht)
@receiver(pre_save, sender=Photo)
@receiver(pre_save, sender=Accommodation)
def replace_item(instance, sender, **kwargs):
    for field in instance._meta.get_fields():
        try:
            if field.get_internal_type() == 'FileField':
                exist_item = getattr(sender.objects.get(id=instance.id), field.__dict__['name'])
                new_item = getattr(instance, field.__dict__['name'])
                if exist_item != new_item:
                    exist_item.delete(save=False)
        except:
            pass



@receiver(pre_delete, sender=Yacht)
@receiver(pre_delete, sender=Photo)
@receiver(pre_delete, sender=Accommodation)
def del_item(instance, **kwargs):
    for field in instance._meta.get_fields():
        try:
            if field.get_internal_type() == 'FileField':
                item = getattr(instance, field.__dict__['name'])
                item.delete(save=False)
        except:
            pass