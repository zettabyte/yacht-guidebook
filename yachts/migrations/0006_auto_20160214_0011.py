# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-02-13 22:11
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('yachts', '0005_auto_20160213_2310'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accommodation',
            name='yacht',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='accommodation', to='yachts.Yacht'),
        ),
    ]
