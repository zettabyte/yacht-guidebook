# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-02-13 21:07
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('yachts', '0002_auto_20160212_1841'),
    ]

    operations = [
        migrations.CreateModel(
            name='Accommodation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cabins', models.SmallIntegerField()),
                ('wc', models.SmallIntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Engine',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount', models.SmallIntegerField()),
                ('power', models.SmallIntegerField()),
            ],
        ),
        migrations.AlterModelOptions(
            name='yacht',
            options={'ordering': ('title',)},
        ),
        migrations.RemoveField(
            model_name='yacht',
            name='cabins',
        ),
        migrations.RemoveField(
            model_name='yacht',
            name='wc',
        ),
        migrations.AddField(
            model_name='engine',
            name='yacht',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='yachts.Yacht'),
        ),
        migrations.AddField(
            model_name='accommodation',
            name='yacht',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='yachts.Yacht'),
        ),
    ]
