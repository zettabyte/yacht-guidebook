# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-12 13:23
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('yachts', '0016_auto_20160312_1453'),
    ]

    operations = [
        migrations.CreateModel(
            name='Catalogue',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.URLField()),
                ('order_id', models.SmallIntegerField()),
            ],
            options={
                'ordering': ('order_id',),
            },
        ),
        migrations.AddField(
            model_name='yacht',
            name='website',
            field=models.URLField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='catalogue',
            name='yacht',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='yachts.Yacht'),
        ),
    ]
