# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-15 14:07
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachts', '0030_auto_20160315_0014'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Catalogue',
            new_name='Link',
        ),
    ]
