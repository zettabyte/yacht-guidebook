# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-04-04 10:01
from __future__ import unicode_literals

from django.db import migrations, models
import yachts.models


class Migration(migrations.Migration):

    dependencies = [
        ('yachts', '0035_auto_20160403_1405'),
    ]

    operations = [
        migrations.AddField(
            model_name='accommodation',
            name='plan_img',
            field=models.ImageField(null=True, upload_to=yachts.models.Accommodation.plan_img_path),
        ),
    ]
