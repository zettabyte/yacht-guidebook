import random

from rest_framework.test import APITestCase
from rest_framework import status

from django.utils.text import slugify

from .models import Brand, Type, Yacht, Accommodation


def create_test_brand(title):
    return Brand.objects.create(title=title, slug=slugify(title))


def create_test_type(title):
    return Type.objects.create(title=title, slug=slugify(title))


def create_test_yacht(title, published=False, brand=None, yacht_type=None, length=None, draft=None):
    return Yacht.objects.create(title=title,
                                published=published,
                                slug=slugify(title),
                                card_img='test_data/azimut-46.jpg',
                                brand= brand if brand else random.choice(Brand.objects.all()),
                                type=yacht_type if yacht_type else random.choice(Type.objects.all()),
                                length_over_all=length if length else random.randint(20, 80),
                                draft=draft)


def create_test_accommodation(yacht=None, cabins=None, wc=None):
    return Accommodation.objects.create(yacht=yacht if yacht else random.choice(Yacht.objects.all()),
                                        cabins=cabins if cabins else random.randint(1, 8),
                                        wc=wc if wc else random.randint(1, 8))


class YachtTests(APITestCase):
    def setUp(self):
        create_test_brand('Bavaria')
        create_test_type('Sailing yacht')
        create_test_yacht('Bavaria 31 Cruiser')
        yacht = create_test_yacht('Bavaria 34 Cruiser', published=True)
        create_test_accommodation(yacht=yacht, cabins=2)

    def test_yacht_was_published(self):
        self.assertTrue(len(Yacht.objects.all()) == 2)
        self.assertTrue(len(Yacht.objects.published().all()) == 1)

    def test_yacht_list(self):
        response = self.client.get('/api/yachts')
        self.assertEqual(response.data['results'][0]['slug'], 'bavaria-34-cruiser')

    def test_filter_yacht_list(self):
        response = self.client.get('/api/yachts?brand=bavaria&type=sailing-yacht&cabins=2')
        self.assertEqual(response.data['results'][0]['slug'], 'bavaria-34-cruiser')
        response = self.client.get('/api/yachts?brand=fake-brand-slug')
        self.assertTrue(len(response.data['results']) == 0)

    def test_search_yacht(self):
        response = self.client.get('/api/yachts?search=34')
        self.assertEqual(response.data['results'][0]['slug'], 'bavaria-34-cruiser')
        response = self.client.get('/api/yachts?search=fake-yacht-title')
        self.assertTrue(len(response.data['results']) == 0)

    def test_yacht_retrieve(self):
        response = self.client.get('/api/yachts/bavaria-34-cruiser')
        self.assertEqual(response.data['slug'], 'bavaria-34-cruiser')
        response = self.client.get('/api/yachts/fake-yacht-slug')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
