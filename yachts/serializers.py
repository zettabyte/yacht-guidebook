from rest_framework import serializers
from .models import Yacht, Brand, Accommodation, Engine, Photo, Video, Link


class BrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = ('title', 'website')


class AccommodationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Accommodation
        fields = ('cabins', 'berths', 'wc', 'plan_img')


class EngineSerializer(serializers.ModelSerializer):
    class Meta:
        model = Engine
        fields = ('amount', 'title', 'power', 'cruising_speed', 'max_speed', 'fuel_consumption')


class PhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Photo
        fields = ('img', 'thumbnail', 'order_id')


class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Video
        fields = ('code', 'order_id')


class LinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Link
        fields = ('title', 'url', 'order_id')


class YachtListSerializer(serializers.ModelSerializer):
    brand = serializers.SlugRelatedField(slug_field='title', read_only=True)
    type = serializers.SlugRelatedField(slug_field='title', read_only=True)
    cabins = serializers.SerializerMethodField()

    class Meta:
        model = Yacht
        fields = ('slug', 'title', 'card_img', 'brand', 'type', 'length_over_all', 'draft', 'cabins')

    def get_cabins(self, obj):
        cabins = Accommodation.objects.filter(yacht=obj.id).values_list('cabins', flat=True)
        return sorted(list(set(cabins)))


class YachtDetailSerializer(serializers.ModelSerializer):
    brand = BrandSerializer(read_only=True)
    type = serializers.SlugRelatedField(slug_field='title', read_only=True)
    accommodation_set = AccommodationSerializer(many=True, read_only=True)
    engine_set = EngineSerializer(many=True, read_only=True)
    photo_set = PhotoSerializer(many=True, read_only=True)
    video_set = VideoSerializer(many=True, read_only=True)
    link_set = LinkSerializer(many=True, read_only=True)

    class Meta:
        model = Yacht
        fields = ('title', 'slug', 'card_img', 'brand', 'type', 'website', 'accommodation_set', 'length_over_all', 'beam', 'draft',
                  'weight_dry', 'engine_set', 'amount_of_fuel_tanks', 'fuel_tank', 'amount_of_water_tanks', 'water_tank',
                  'photo_set', 'video_set', 'link_set')




