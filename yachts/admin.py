from django.contrib import admin
from django import forms
from django.db import models
from django.utils.safestring import mark_safe

from .models import Type, Brand, Yacht, Engine, Accommodation, Photo, Video, Link


class AdminImagePreviewWidget(forms.FileInput):
    def __init__(self, attrs={}):
        super(AdminImagePreviewWidget, self).__init__(attrs)

    def render(self, name, value, attrs=None):
        output = []
        if value and hasattr(value, "url"):
            output.append(('<a target="_blank" href="%s">'
                           '<img src="%s" style="height: 60px;" /></a> '
                           % (value.url, value.url)))
        output.append(super(AdminImagePreviewWidget, self).render(name, value, attrs))
        return mark_safe(''.join(output))


class EngineInline(admin.StackedInline):
    model = Engine
    extra = 0


class AccommodationInline(admin.TabularInline):
    model = Accommodation
    formfield_overrides = {models.ImageField: {'widget': AdminImagePreviewWidget}}
    extra = 0


class PhotoInline(admin.TabularInline):
    model = Photo
    formfield_overrides = {models.ImageField: {'widget': AdminImagePreviewWidget}}
    fields = ('img', 'order_id')
    extra = 0


class VideoInline(admin.TabularInline):
    model = Video
    extra = 0


class LinkInline(admin.TabularInline):
    model = Link
    extra = 0


class PrepTitle(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}


class YachtAdmin(PrepTitle, admin.ModelAdmin):
    save_on_top = True
    formfield_overrides = {models.ImageField: {'widget': AdminImagePreviewWidget}}
    prepopulated_fields = {'slug': ('title', 'built_year')}
    list_display = ('title', 'brand', 'type', 'published')
    list_filter = ('brand', 'type', 'published')
    list_editable = ('published',)
    fieldsets = (
        (None, {'fields': ('title', 'published', 'card_img', 'built_year', 'slug', 'brand', 'type', 'website')}),
        ('Dimensions', {'classes': ('wide',),
                        'fields': ('length_over_all', 'beam', 'draft', 'weight_dry')}),
        ('Tanks', {'classes': ('wide',),
                   'fields': ('amount_of_fuel_tanks', 'fuel_tank', 'amount_of_water_tanks', 'water_tank')})
    )
    inlines = (EngineInline, AccommodationInline, PhotoInline, VideoInline, LinkInline)


admin.site.register(Yacht, YachtAdmin)
admin.site.register(Brand, PrepTitle)
admin.site.register(Type, PrepTitle)
