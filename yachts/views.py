from django.db.models import Q
from django.shortcuts import get_object_or_404

from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import filters
from rest_framework.pagination import PageNumberPagination

import django_filters

from .serializers import YachtListSerializer, YachtDetailSerializer
from .models import Yacht, Type, Brand, Accommodation

from yacht_filtering.models import RangeFilterOptions


class YachtResultsPagination(PageNumberPagination):
    page_size = 12
    def get_paginated_response(self, data):
        return Response({
            'count': self.page.paginator.count,
            'page_size': self.page_size,
            'results': data
        })


def range_filtering(queryset, value, field):
    q_res = Q()
    for v in value:
        rangeVal = RangeFilterOptions.objects.get(slug=v)
        if (rangeVal.min_value != None) & (rangeVal.max_value != None):
            q_res = q_res | Q(**{field + '__gte': rangeVal.min_value}) & Q(**{field + '__lte': rangeVal.max_value})
        elif (rangeVal.min_value != None) & (rangeVal.max_value == None):
            q_res = q_res | Q(**{field + '__gte': rangeVal.min_value})
        elif (rangeVal.min_value == None) & (rangeVal.max_value != None):
            q_res = q_res | Q(**{field + '__lte': rangeVal.max_value})
    queryset = queryset.filter(q_res)
    return queryset


class YachtFilter(filters.FilterSet):
    brand = django_filters.MultipleChoiceFilter(name='brand__slug', choices=Brand.objects.values_list('slug', 'title'))
    type = django_filters.MultipleChoiceFilter(name='type__slug', choices=Type.objects.values_list('slug', 'title'))
    cabins = django_filters.MultipleChoiceFilter(name='accommodation__cabins', choices=tuple((i, i) for i in Accommodation.objects.values_list('cabins', flat=True).distinct().order_by('cabins')))
    length = django_filters.MultipleChoiceFilter(action=lambda queryset, value: range_filtering(queryset, value, field='length_over_all'), choices=RangeFilterOptions.objects.values_list('slug', 'title'))
    draft = django_filters.MultipleChoiceFilter(action=lambda queryset, value: range_filtering(queryset, value, field='draft'), choices=RangeFilterOptions.objects.values_list('slug', 'title'))

    class Meta:
        model = Yacht
        fields = ['brand', 'type', 'cabins', 'length', 'draft']


class YachtViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Yacht.objects.published()
    pagination_class = YachtResultsPagination
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, filters.DjangoFilterBackend)
    filter_class = YachtFilter
    search_fields = ('title',)
    ordering_fields = ('length_over_all', 'title')

    def list(self, request, *args, **kwargs):
        filtered_queryset = self.filter_queryset(self.queryset)
        page = self.paginate_queryset(filtered_queryset)
        serializer = YachtListSerializer(page, many=True)
        return self.get_paginated_response(serializer.data)

    def retrieve(self, request, pk=None, *args, **kwargs):
        return Response(YachtDetailSerializer(get_object_or_404(self.queryset, slug=pk)).data)