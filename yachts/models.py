import os

from django.db import models

from utils import img_utils


class Brand(models.Model):
    title = models.CharField(max_length=255)
    slug = models.SlugField(unique=True)
    website = models.URLField(null=True, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('title',)


class Type(models.Model):
    title = models.CharField(max_length=255)
    slug = models.SlugField(unique=True)

    def __str__(self):
        return self.title


class PublishedYachtManager(models.Manager):
    def published(self):
        return super(PublishedYachtManager, self).get_queryset().filter(published=True)


class Yacht(models.Model):
    def img_path(self, filename):
        return os.path.join(self._meta.app_label, self._meta.model_name, self.slug, filename)

    title = models.CharField(max_length=255)
    published = models.BooleanField(default=False)
    card_img = models.ImageField(upload_to=img_path)
    built_year = models.PositiveSmallIntegerField(null=True, blank=True)
    slug = models.SlugField(unique=True)
    brand = models.ForeignKey(Brand)
    type = models.ForeignKey(Type)
    length_over_all = models.FloatField()
    beam = models.FloatField(null=True, blank=True)
    draft = models.FloatField(null=True, blank=True)
    weight_dry = models.FloatField(null=True, blank=True)
    amount_of_fuel_tanks = models.PositiveSmallIntegerField(null=True, blank=True)
    fuel_tank = models.FloatField(null=True, blank=True)
    amount_of_water_tanks = models.SmallIntegerField(null=True, blank=True)
    water_tank = models.FloatField(null=True, blank=True)
    website = models.URLField(null=True, blank=True)

    objects = PublishedYachtManager()

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not os.path.isfile(self.card_img.path):
            self.card_img = img_utils.resize_and_crop(710, 490, self.card_img.file)
        super(Yacht, self).save(*args, **kwargs)

    class Meta:
        ordering = ('title',)


class Accommodation(models.Model):
    def plan_img_path(self, filename):
        return os.path.join(self._meta.app_label, self._meta.model_name, self.yacht.slug, 'plan', filename)

    yacht = models.ForeignKey(Yacht)
    cabins = models.PositiveSmallIntegerField()
    berths = models.CharField(max_length=255, null=True, blank=True)
    wc = models.PositiveSmallIntegerField()
    plan_img = models. ImageField(upload_to=plan_img_path, null=True, blank=True)

    class Meta:
        ordering = ('cabins', 'wc')


class Engine(models.Model):
    yacht = models.ForeignKey(Yacht)
    amount = models.PositiveSmallIntegerField()
    title = models.CharField(max_length=255, null=True, blank=True)
    power = models.PositiveSmallIntegerField()
    cruising_speed = models.FloatField(null=True, blank=True)
    max_speed = models.FloatField(null=True, blank=True)
    fuel_consumption = models.FloatField(null=True, blank=True)


class Photo(models.Model):
    def img_path(self, filename):
        return os.path.join(self._meta.app_label, self._meta.model_name, self.yacht.slug, filename)

    def thumbnail_path(self, filename):
        return os.path.join(self._meta.app_label, self._meta.model_name, self.yacht.slug, 'thumbnail', filename)

    yacht = models.ForeignKey(Yacht)
    thumbnail = models.ImageField(upload_to=thumbnail_path)
    img = models.ImageField(upload_to=img_path)
    order_id = models.SmallIntegerField(null=True, blank=True)

    def save(self, *args, **kwargs):
        if not os.path.isfile(self.img.path):
            self.thumbnail = img_utils.resize_and_crop(230, 120, self.img.file)
        super(Photo, self).save(*args, **kwargs)

    class Meta:
        ordering = ('order_id',)


class Video(models.Model):
    yacht = models.ForeignKey(Yacht)
    code = models.CharField(max_length=255)
    order_id = models.SmallIntegerField()

    class Meta:
        ordering = ('order_id',)


class Link(models.Model):
    yacht = models.ForeignKey(Yacht)
    title = models.CharField(max_length=255)
    url = models.URLField()
    order_id = models.SmallIntegerField()

    class Meta:
        ordering = ('order_id',)