from django.apps import AppConfig


class YachtsConfig(AppConfig):
    name = 'yachts'

    def ready(self):
        import yachts.signals