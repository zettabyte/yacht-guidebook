import io, math
from PIL import Image

from django.core.files.uploadedfile import InMemoryUploadedFile


def resize_and_crop(width, crop_height, image):
    img = Image.open(image)

    img_width, img_height = img.size

    if img_width/img_height > width/crop_height:
        img.thumbnail(((crop_height/img_height)*img_width, crop_height))
        img_width, img_height = img.size
        margin_crop = (img_width - width)/2
        left = math.ceil(margin_crop)
        top = 0
        right = img_width - math.floor(margin_crop)
        bottom = img_height
        img = img.crop((left, top, right, bottom))
    else:
        img.thumbnail((width, img_height))
        img_width, img_height = img.size
        margin_crop = (img_height - crop_height)/2
        left = 0
        top = math.ceil(margin_crop)
        right = img_width
        bottom = img_height - math.floor(margin_crop)
        img = img.crop((left, top, right, bottom))

    io_img = io.BytesIO()
    img.save(io_img, format='JPEG')

    return InMemoryUploadedFile(io_img, None, image.name, 'image/jpeg', io_img.getbuffer().nbytes, None)
