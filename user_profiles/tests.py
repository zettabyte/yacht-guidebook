from rest_framework.test import APITransactionTestCase, APITestCase
from rest_framework.authtoken.models import Token
from rest_framework import status

from django.contrib.auth.models import User


def create_test_user(username='test_user', email='test_user@test.com', password='test_password'):
    user = User.objects.create_user(username=username, email=email, password=password)
    Token.objects.create(user=user)
    return user


class UserProfileTests(APITestCase):
    def setUp(self):
        self.user = create_test_user('user0', 'user0@test.com'), create_test_user('user1', 'user1@test.com')

    def test_get_profile_unauthorized_client(self):
        response = self.client.get('/api/users/'+self.user[0].username)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_profile_authorized_not_owner_client(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + Token.objects.get(user=self.user[1]).key)
        response = self.client.get('/api/users/'+self.user[0].username)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_get_profile_authorized_owner_client(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + Token.objects.get(user=self.user[0]).key)
        response = self.client.get('/api/users/'+self.user[0].username)
        self.assertEqual(response.data['username'], self.user[0].username)

    def test_update_profile_unauthorized_client(self):
        response = self.client.patch('/api/users/'+self.user[0].username, data={'email': 'newemail@test.com'})
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_profile_authorized_not_owner_client(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + Token.objects.get(user=self.user[1]).key)
        response = self.client.patch('/api/users/'+self.user[0].username, data={'email': 'newemail@test.com'})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update_profile_authorized_owner_client(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + Token.objects.get(user=self.user[0]).key)
        self.client.patch('/api/users/'+self.user[0].username, data={'email': 'newemail@test.com'})
        self.assertEqual(User.objects.get(username=self.user[0].username).email, 'newemail@test.com')

    def test_create_profile(self):
        data = {'username':'test_user', 'email': 'test_user@test.com', 'password': 'test_password'}
        response = self.client.post('/api/users', data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_get_token_registered_client(self):
        response = self.client.post('/api-token-auth', data={'username': 'user0', 'password': 'test_password'})
        self.assertTrue(len(response.data['token']) > 0)

    def test_get_token_unregistered_client(self):
        response = self.client.post('/api-token-auth', data={'username': 'unregistered', 'password': 'test_password'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)