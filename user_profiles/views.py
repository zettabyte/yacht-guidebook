from django.contrib.auth import get_user_model

from rest_framework import mixins, viewsets

from .serializers import UserSerializer
from .permissions import IsOwnerOfObject


User = get_user_model()


class UserViewSet(mixins.RetrieveModelMixin, mixins.CreateModelMixin, mixins.UpdateModelMixin, viewsets.GenericViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    lookup_field = 'username'
    permission_classes = (IsOwnerOfObject,)

