from rest_framework import permissions


class IsOwnerOrReadAndCreateOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        methods = permissions.SAFE_METHODS + ('POST',)
        return (
            request.method in methods or
            request.user and
            request.user.is_authenticated()
        )

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return obj == request.user


class IsOwnerOfObject(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj == request.user
