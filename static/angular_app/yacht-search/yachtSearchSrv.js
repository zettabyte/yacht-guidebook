'use strict';


var yachtSearch = angular.module('yachtGuidebook.yachtSearch');


yachtSearch.factory('YachtListSrv', ['$resource', function ($resource) {
    return $resource('api/yachts', {}, {
        query: {method: 'GET', isArray: false}
    });
}]);


yachtSearch.factory('FiltersOptionsSrv', ['$resource', function ($resource) {
    return $resource('api/filters/options', {}, {
        query: {method: 'GET', isArray: false}
    });
}]);


yachtSearch.factory('FiltersObjectSrv', ['$state', function ($state) {
    var params = {filters: {}, ordering: {options: {}, value: null}, searching: {value: null}};

    var initFiltersState = function () {
        for (var option in params.filters) {
            angular.forEach(params.filters[option], function (item) {
                item.state = false;
                if ($state.params[option] && $state.params[option].indexOf(item.slug) > -1) {
                    item.state = true;
                }
            });
        }
        return params.filters;
    };

    var setFilterOptions = function (obj) {
        angular.extend(params.filters, obj);
    };

    var setOrderingOptions = function (obj) {
        angular.extend(params.ordering.options, obj);
    };

    var initOrdering = function () {
        params.ordering.value = $state.params['ordering'];
        return params.ordering
    };

    var initSearching = function () {
        params.searching.value = $state.params['search'];
        return params.searching
    };

    var getFilter = function () {
        return params;
    };

    return {
        setFilterOptions: setFilterOptions,
        setOrderingOptions: setOrderingOptions,
        getFilter: getFilter,
        initFiltersState: initFiltersState,
        initOrdering: initOrdering,
        initSearching: initSearching
    }
}]);


yachtSearch.factory('UserFiltersSrv', ['$resource', function ($resource) {
    return $resource('api/filters/user-filters/:id', {}, {
        'query': {method: 'GET', isArray: true},
        'save': {method: 'POST'},
        'delete': {method: 'DELETE'}
    });
}]);
