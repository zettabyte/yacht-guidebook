'use strict';


var yachtSearch = angular.module('yachtGuidebook.yachtSearch');


yachtSearch.controller('YachtSearchCtrl', ['$scope', 'FiltersObjectSrv', 'FiltersOptionsSrv', '$state', function ($scope, FiltersObjectSrv, FiltersOptionsSrv, $state) {
    $scope.isSidebarCollapsed = true;

    $scope.isFilterApplied = function () {
        for (var res = false, i = 0; i < arguments.length; i++) {
            res = res || $state.params[arguments[i]];
        }
        return res;
    };
}]);


yachtSearch.controller('YachtListCtrl', ['$scope', '$state', 'YachtListSrv', 'yachtList', 'StoreLastStateSrv', '$anchorScroll', function ($scope, $state, YachtListSrv, yachtList, StoreLastStateSrv, $anchorScroll) {
    StoreLastStateSrv.setState('yachtSearch', $state.current, $state.params);

    $scope.yachts = yachtList.results;
    $scope.count = yachtList.count;
    $scope.pageSize = yachtList.page_size;

    $scope.currentPage = 1;
    if ($state.params['page']) {
        $scope.currentPage = $state.params['page'];
    }

    $scope.pageChanged = function () {
        if ($scope.currentPage > 1) {
            $state.go('.', {page: $scope.currentPage});
        } else {
            $state.go('.', {page: null});
        }
        $anchorScroll();
    };
}]);


yachtSearch.controller('FilterListCtrl', ['$scope', 'FiltersObjectSrv', 'filterOptions', function ($scope, FiltersObjectSrv, filterOptions) {
    $scope.isCollapsed = {
        'brands': true,
        'types': true,
        'cabins': true,
        'length': true,
        'draft': true
    };

    FiltersObjectSrv.setFilterOptions(filterOptions.filters);
    FiltersObjectSrv.setOrderingOptions(filterOptions.ordering);
    FiltersObjectSrv.initFiltersState();
    FiltersObjectSrv.initOrdering();

    $scope.filters = FiltersObjectSrv.getFilter().filters;
}]);


yachtSearch.controller('UserFiltersCtrl', ['$scope', 'UserFiltersSrv', 'CurrentUserSrv', 'FiltersObjectSrv', '$state', 'ErrorAlertSrv', function ($scope, UserFiltersSrv, CurrentUserSrv, FiltersObjectSrv, $state, ErrorAlertSrv) {
    $scope.currentUser = CurrentUserSrv.profile;
    $scope.isCollapsed = {saveFilter: true, userFilters: false};

    var loadUserFilters = function () {
        UserFiltersSrv.query({},
            function success(response) {
                $scope.userFilters = response;
            }
        );
    };

    $scope.applyUserFilters = function (index) {
        $state.go($state.current, angular.fromJson($scope.userFilters[index].json), {inherit: false}).then(
            function () {
                FiltersObjectSrv.initFiltersState();
                FiltersObjectSrv.initOrdering();
                FiltersObjectSrv.initSearching();
            }
        );
    };

    $scope.saveUserFilter = function (title) {
        UserFiltersSrv.save(
            {title: $scope.userFiltersTitle, json: angular.toJson($state.params)},
            function success() {
                $scope.userFiltersTitle = '';
                loadUserFilters();
            },
            function error() {
                ErrorAlertSrv.showAlert();
            }
        );
    };

    $scope.deleteUserFilters = function (index) {
        UserFiltersSrv.delete(
            {id: $scope.userFilters[index].id},
            function success() {
                loadUserFilters();
            },
            function error() {
                ErrorAlertSrv.showAlert();
            }
        );
    };

    if ($scope.currentUser.isLoggedIn) {
        loadUserFilters();
    }
}]);


yachtSearch.controller('OrderByCtrl', ['$scope', 'FiltersObjectSrv', '$state', function ($scope, FiltersObjectSrv, $state) {
    $scope.ordering = FiltersObjectSrv.getFilter().ordering;

    $scope.applyOrder = function () {
        $state.go('yachtSearch.yachtList', {ordering: $scope.ordering.value}, {reload: 'yachtSearch.yachtList'});
    };
}]);


yachtSearch.controller('ActiveFiltersCtrl', ['$scope', 'CurrentUserSrv', 'FiltersObjectSrv', '$state', function ($scope, CurrentUserSrv, FiltersObjectSrv, $state) {
    $scope.filters = FiltersObjectSrv.getFilter().filters;

    $scope.searchRequest = FiltersObjectSrv.initSearching();

    $scope.removeSearching = function () {
        $state.go($state.current, {search: null, page: null}, {reload: 'yachtSearch.yachtList'}).then(function () {
            FiltersObjectSrv.initSearching();
        });
    };

    $scope.removeFilter = function (key, value) {
        var stateParam = {};
        stateParam[key] = $state.params[key];
        if (stateParam[key] && stateParam[key].indexOf(value) > -1) {
            stateParam[key].splice(stateParam[key].indexOf(value), 1);
        }
        stateParam['page'] = null;
        $state.go($state.current, stateParam, {reload: 'yachtSearch.yachtList'});
    };

    $scope.filtersReset = function () {
        $state.go($state.current, {ordering: $state.params['ordering']}, {
            reload: 'yachtSearch.yachtList',
            inherit: false
        }).then(function () {
            FiltersObjectSrv.initFiltersState();
            FiltersObjectSrv.initOrdering();
            FiltersObjectSrv.initSearching();
        });
    }
}]);