'use strict';

var yachtSearch = angular.module('yachtGuidebook.yachtSearch');

yachtSearch.directive('ygbFilterListGroup', function () {
    return {
        restrict: 'AE',
        templateUrl: 'static/angular_app/yacht-search/filter-list-group.html',
        scope: {
            isCollapsed: '=',
            filterList: '=',
            filterName: '@',
            groupTitle: '@'
        },
        controller: ['$scope', '$state', function ($scope, $state) {
            $scope.filterStateChanged = function (filterName, filterValue, state) {

                var stateParam = {};
                stateParam[filterName] = $state.params[filterName];


                if (stateParam[filterName]) {
                    if (stateParam[filterName].indexOf(filterValue) > -1) {
                        stateParam[filterName].splice(stateParam[filterName].indexOf(filterValue), 1);
                    } else {
                        stateParam[filterName].push(filterValue);
                    }
                } else {
                    stateParam[filterName] = [].concat(filterValue);
                }

                stateParam['page'] = null;

                $state.go($state.current, stateParam, {reload: 'yachtSearch.yachtList'});

            };
        }]
    };
});
