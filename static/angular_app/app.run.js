'use strict';


var appRun = angular.module('appRun', []);

appRun.run(['$rootScope', '$state', function ($rootScope, $state) {
    $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
        if (error.status == 404) {
            $state.go('404', {}, {location: false});
        }
    });
}]);