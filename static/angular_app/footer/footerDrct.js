'use strict';

var footer = angular.module('yachtGuidebook.footer');

footer.directive('ygbFooter', function(){
    return {
        restrict:'AE',
        templateUrl: 'static/angular_app/footer/footer.html',
        controller: ['$scope', function($scope){
            $scope.currentDate = new Date();
        }]
    };
});
