'use strict';

var headMeta = angular.module('yachtGuidebook.headMeta');

headMeta.controller('HeadMetaCtrl', ['$scope', 'HeadMetaSrv', function($scope, HeadMetaSrv){
    $scope.meta = HeadMetaSrv.meta;
}]);
