'use strict';

var headMeta = angular.module('yachtGuidebook.headMeta');

headMeta.factory('HeadMetaSrv', [function(){
    var siteName = 'Yacht Guidebook';
    var description = '';

    var setTitle = function(value) {
        if (value) {
            meta.title = value + ' - ' + siteName;
        } else {
            meta.title = siteName;
        }
    };

    var setDescription = function(value){
        meta.description = value;
    };

    var meta = {
        title: siteName,
        description: '',
        setTitle: setTitle,
        setDescription: setDescription
    };

    return {
        meta: meta
    }

}]);
