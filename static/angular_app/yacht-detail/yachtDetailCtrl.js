'use strict';

var yachtDetail = angular.module('yachtGuidebook.yachtDetail');


yachtDetail.controller('YachtDetailCtrl', ['$scope', '$stateParams', '$sce', 'YachtDetailSrv', 'yachtDetails', '$uibModal', function ($scope, $stateParams, $sce, YachtDetailSrv, yachtDetails, $uibModal) {
    $scope.yacht = yachtDetails;

    $scope.playVideo = function(videoCode){
        $uibModal.open({
            templateUrl: 'static/angular_app/yacht-detail/youtube-player.html',
            size: 'lg',
            scope: $scope,
            controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                $scope.videoCode = videoCode;
                $scope.ok = function () {
                    $uibModalInstance.close();
                };
            }]
        });
    };

    $scope.trustAsResourceUrl = function (url) {
        return $sce.trustAsResourceUrl(url);
    };
}]);


yachtDetail.controller('YachtRatingCtrl', ['$scope', '$stateParams', 'RatingSrv', 'CurrentUserSrv', 'ErrorAlertSrv', function ($scope, $stateParams, RatingSrv, CurrentUserSrv, ErrorAlertSrv) {
    $scope.currentUser = CurrentUserSrv.profile;
    $scope.userRate = {};

    var loadAvgRating = function () {
        RatingSrv.avg(
            {'yachtSlug': $stateParams.yachtSlug},
            function success(response) {
                $scope.avgRating = response;
            }
        );
    };

    var loadUserRate = function () {
        RatingSrv.get(
            {'yachtSlug': $stateParams.yachtSlug},
            function success(response) {
                $scope.userRate = response;
            },
            function error(errorResponse) {
                if (errorResponse.status === 404) {
                    $scope.userRate = null;
                }
            }
        );
    };

    var loadRating = function () {
        loadAvgRating();
        if ($scope.currentUser.isLoggedIn) {
            loadUserRate();
        }
    };

    $scope.userRateUpdated = function () {
        if ($scope.userRate.yacht) {
            RatingSrv.update(
                {'yachtSlug': $stateParams.yachtSlug, 'score': $scope.userRate.score},
                function success(response) {
                    loadRating();
                },
                function error() {
                    ErrorAlertSrv.showAlert();
                }
            );
        } else {
            RatingSrv.save(
                {'yacht': $stateParams.yachtSlug, 'score': $scope.userRate.score},
                function success(response) {
                    loadRating();
                },
                function error() {
                    ErrorAlertSrv.showAlert();
                }
            );
        }
    };

    loadRating();
}]);


yachtDetail.controller('CommentListCtrl', ['$scope', '$stateParams', 'CommentsSrv', 'CurrentUserSrv', function ($scope, $stateParams, CommentsSrv, CurrentUserSrv) {
    $scope.editFormExpand = {};
    $scope.alertShow = {};
    $scope.currentUser = CurrentUserSrv.profile;

    var loadComments = function () {
        CommentsSrv.query(
            {'yacht__slug': $stateParams.yachtSlug},
            function success(response) {
                $scope.comments = response;
            }
        );
    };

    $scope.closeAlert = function (alertObj) {
        alertObj.state = false;
    };

    $scope.$on('createSuccessEvent', function () {
        loadComments();
        $scope.alertShow['addComment'] = {state: true, type: 'success', msg: 'Comment was updated successfully!'};
    });

    $scope.$on('createRejectEvent', function () {
        $scope.alertShow['addComment'] = {state: true, type: 'danger', msg: 'Comment text cannot be empty!'};
    });

    $scope.$on('deleteSuccessEvent', loadComments);

    $scope.$on('updateSuccessEvent', function (event, commentId) {
        loadComments();
        $scope.alertShow[commentId] = {state: true, type: 'success', msg: 'Comment was updated successfully!'};
    });

    $scope.$on('updateRejectEvent', function (event, commentId) {
        $scope.alertShow[commentId] = {state: true, type: 'danger', msg: 'Comment text cannot be empty!'};
    });


    $scope.$on('closeEdtFormEvent', function (event, commentId) {
        $scope.editFormExpand[commentId] = false;
    });

    loadComments();
}]);


yachtDetail.controller('AddNewCommentCtrl', ['$scope', '$stateParams', '$uibModal', 'CommentsSrv', 'ErrorAlertSrv', function ($scope, $stateParams, $uibModal, CommentsSrv, ErrorAlertSrv) {
    $scope.saveComment = function () {
        if ($scope.commentText.length == 0) {
            $scope.$emit('createRejectEvent');
        } else {
            CommentsSrv.save({'yacht': $stateParams.yachtSlug, 'text': $scope.commentText},
                function success(response) {
                    $scope.reset();
                    $scope.$emit('createSuccessEvent');
                },
                function error() {
                    ErrorAlertSrv.showAlert();
                }
            );
        }
    };

    $scope.addComment = function (userLoggedIn) {
        if (userLoggedIn) {
            $scope.formCollapsed = false;
        } else {
            $uibModal.open({
                templateUrl: 'static/angular_app/yacht-detail/please-login.html',
                size: 'md',
                controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                    $scope.ok = function () {
                        $uibModalInstance.close();
                    };
                }]
            });
        }
    };

    $scope.reset = function () {
        $scope.formCollapsed = true;
        $scope.commentText = '';
    };

    $scope.reset();
}]);


yachtDetail.controller('DeleteCommentCtrl', ['$scope', '$uibModal', 'CommentsSrv', 'ErrorAlertSrv', function ($scope, $uibModal, CommentsSrv, ErrorAlertSrv) {
    $scope.deleteComment = function (commentId) {
        var modalInstance = $uibModal.open({
            templateUrl: 'static/angular_app/yacht-detail/confirm-delete.html',
            size: 'md',
            controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                $scope.ok = function () {
                    $uibModalInstance.close();
                };
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }]
        });

        modalInstance.result.then(
            function ok() {
                CommentsSrv.delete({commentId: commentId},
                    function success(response) {
                        $scope.$emit('deleteSuccessEvent');
                    },
                    function error() {
                        ErrorAlertSrv.showAlert();
                    }
                );
            }
        );
    };
}]);

yachtDetail.controller('EditCommentCtrl', ['$scope', 'CommentsSrv', 'ErrorAlertSrv', function ($scope, CommentsSrv, ErrorAlertSrv) {
    $scope.commentText = {};

    $scope.cancelUpdateComment = function (commentId, oldValue) {
        $scope.commentText[commentId] = oldValue;
        $scope.$emit('closeEdtFormEvent', commentId);
    };

    $scope.submitUpdateComment = function (commentId) {
        if ($scope.commentText[commentId].length == 0) {
            $scope.$emit('updateRejectEvent', commentId);
        } else {
            CommentsSrv.update({commentId: commentId, 'text': $scope.commentText[commentId]},
                function success(response) {
                    $scope.$emit('closeEdtFormEvent', commentId);
                    $scope.$emit('updateSuccessEvent', commentId);
                },
                function error() {
                    ErrorAlertSrv.showAlert();
                }
            );
        }
    }
}]);
