'use strict';

var yachtDetail = angular.module('yachtGuidebook.yachtDetail');

yachtDetail.factory('YachtDetailSrv', ['$resource', function($resource){
    return $resource('api/yachts/:yachtSlug', {}, {
        get:{method: 'GET', isArray: false}
    });
}]);


yachtDetail.factory('RatingSrv', ['$resource', function($resource){
    return $resource('api/rating/:yachtSlug', {yachtSlug: '@yachtSlug'}, {
        'get': {method: 'GET', isArray: false},
        'save': {method:'POST'},
        'update': {method:'PATCH'},
        'avg': {method: 'GET', url: 'api/rating/:yachtSlug/avg_score'}
    });
}]);


yachtDetail.factory('CommentsSrv', ['$resource', function($resource){
    return $resource('api/comments/:commentId', {commentId: '@commentId'}, {
        'get': {method: 'GET', isArray: false},
        'query': {method: 'GET', isArray: true},
        'save': {method:'POST'},
        'delete': {method:'DELETE'},
        'update': {method:'PATCH'}
    });
}]);