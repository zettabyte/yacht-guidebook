'use strict';

var yachtDetail = angular.module('yachtGuidebook.yachtDetail');


yachtDetail.directive('ygbValueOrNa', function(){
    return {
        restrict: 'AE',
        scope: {
            value: '=',
            classIfNa: '@'
        },
        link: function(scope, el, arrts){
            if (scope.value === null) {
                el.text('n/a');
                el.addClass(scope.classIfNa);
            }
            }
    };
});