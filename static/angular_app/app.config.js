'use strict';


var appConfig = angular.module('appConfig', []);


appConfig.config(['$httpProvider', '$uiViewScrollProvider', function ($httpProvider, $uiViewScrollProvider) {
    $httpProvider.interceptors.push('AddTokenSrv');
    $httpProvider.interceptors.push('LoginRedirectSrv');

    $uiViewScrollProvider.useAnchorScroll();
}]);