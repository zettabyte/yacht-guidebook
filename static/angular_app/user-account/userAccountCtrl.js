'use strict';


var userAccount = angular.module('yachtGuidebook.userAccount');


userAccount.controller('UserLoginCtrl', ['$scope', 'LoginSrv', 'CurrentUserSrv', 'LoginRedirectSrv', function ($scope, LoginSrv, CurrentUserSrv, LoginRedirectSrv) {
    $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams, options) {
        LoginRedirectSrv.setLastPath(fromState, fromParams);
    });

    $scope.userLogin = function () {
        $scope.alerts = [];
        $scope.fieldStatus = {};
        return LoginSrv.post({username: $scope.username, password: $scope.password},
            function success(response) {
                CurrentUserSrv.setProfile($scope.username, response.token);
                LoginRedirectSrv.redirectPostLogin();
            },
            function error(errorResponse) {
                for (var error in errorResponse.data) {
                    $scope.fieldStatus[error] = {error: true};
                    var fieldNameMsg = '';
                    if (error != 'non_field_errors') {
                        fieldNameMsg = error.charAt(0).toUpperCase() + error.slice(1) + ' error! ';
                    }
                    $scope.alerts.push({show: true, msg: fieldNameMsg + errorResponse.data[error][0], type: 'danger'});

                }
            }
        );
    };
}]);


userAccount.controller('RegistrationCtrl', ['$scope', '$state', 'UserProfileSrv', 'LoginSrv', 'CurrentUserSrv', function ($scope, $state, UserProfileSrv, LoginSrv, CurrentUserSrv) {
    $scope.registration = function () {
        $scope.alerts = [];
        $scope.fieldStatus = {};
        return UserProfileSrv.post({username: $scope.username, email: $scope.email, password: $scope.password},
            function success(response) {
                LoginSrv.post({username: $scope.username, password: $scope.password},
                    function success(response) {
                        CurrentUserSrv.setProfile($scope.username, response.token);
                        $scope.registrationSuccess = true;
                    }
                );
            },
            function error(errorResponse) {
                for (var error in errorResponse.data) {
                    $scope.fieldStatus[error] = {error: true};
                    var fieldNameMsg = '';
                    if (error != 'non_field_errors') {
                        fieldNameMsg = error.charAt(0).toUpperCase() + error.slice(1) + ' error! ';
                    }
                    $scope.alerts.push({show: true, msg: fieldNameMsg + errorResponse.data[error][0], type: 'danger'});

                }
            }
        );
    };

}]);


userAccount.controller('ProfileCtrl', ['$scope', 'CurrentUserSrv', 'UserProfileSrv', 'userProfile', function ($scope, CurrentUserSrv, UserProfileSrv, userProfile) {
    $scope.currentUser = CurrentUserSrv.profile;
    $scope.userProfile = userProfile;

    $scope.updateProfile = function () {
        $scope.alerts = [];
        $scope.fieldStatus = {};
        if (!$scope.userProfileForm.$pristine) {
            UserProfileSrv.update(
                {
                    currentUsername: $scope.currentUser.username,
                    email: $scope.userProfile.email,
                    password: $scope.userProfile.password
                },
                function success(response) {
                    $scope.userProfile = response;
                    $scope.alerts.push({
                        show: true,
                        msg: 'Your profile data were successfully changed',
                        type: 'success'
                    });
                    $scope.userProfileForm.$setPristine();
                },
                function error(errorResponse) {
                    for (var error in errorResponse.data) {
                        $scope.fieldStatus[error] = {error: true};
                        $scope.alerts.push({
                            show: true,
                            msg: error.charAt(0).toUpperCase() + error.slice(1) + ' error! ' + errorResponse.data[error][0],
                            type: 'danger'
                        });
                    }
                }
            );

        }
    };
}]);