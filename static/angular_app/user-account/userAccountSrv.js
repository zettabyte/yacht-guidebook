'use strict';

var userAccountSrv = angular.module('yachtGuidebook.userAccount');



userAccountSrv.factory('UserProfileSrv', ['$resource', function($resource){
    return $resource('api/users/:currentUsername', {currentUsername: '@currentUsername'}, {
        get: {method: 'GET'},
        post: {method: 'POST'},
        update: {method: 'PATCH'}
    });
}]);




userAccountSrv.factory('LoginSrv', ['$resource', function($resource){
    return $resource('api-token-auth', {}, {post: {method: 'POST'}});
}]);



userAccountSrv.factory('CurrentUserSrv', ['LocalStorageSrv', function(LocalStorageSrv){

    var USER_PROFILE = 'user_profile';

    var initialize = function () {
        var user = {
            username: '',
            token: '',
            get isLoggedIn() {
                if (this.token) {
                    return true;
                } else {
                    return false;
                }
            }
        };

        var localUser = LocalStorageSrv.get(USER_PROFILE);

        if (localUser) {
            user.username = localUser.username;
            user.token = localUser.token;
        }

        return user;
    };

    var profile = initialize();

    var setProfile = function(username, token) {
        profile.username = username;
        profile.token = token;
        LocalStorageSrv.add(USER_PROFILE, profile)
    };

    var removeProfile = function() {
        profile.username = '';
        profile.token = '';
        LocalStorageSrv.remove(USER_PROFILE);
    };

    return {
        setProfile: setProfile,
        removeProfile: removeProfile,
        profile: profile
    };
}]);



userAccountSrv.factory('AddTokenSrv', ['CurrentUserSrv', '$q', function(CurrentUserSrv, $q){
    var request = function(config){
        if (CurrentUserSrv.profile.isLoggedIn){
            config.headers.Authorization = 'Token ' + CurrentUserSrv.profile.token;
        }
        return $q.when(config);
    };

    return {
        request: request
    };
}]);



userAccountSrv.factory('LoginRedirectSrv', ['$q', '$injector', function($q, $injector){
    var lastState = 'home';
    var lastParams = {};

    var responseError = function(response){
        if(response.status === 401){
            $injector.get('$state').go('login');
        }
        return $q.reject(response);
    };

    var redirectPostLogin = function(){
        if (lastState.abstract) {
            $injector.get('$state').go('home')
        } else {
            $injector.get('$state').go(lastState, lastParams);
        }
    };

    var setLastPath = function(state, params){
        lastState = state;
        lastParams = params;
    };

    return {
        responseError: responseError,
        redirectPostLogin: redirectPostLogin,
        setLastPath: setLastPath
    };
}]);