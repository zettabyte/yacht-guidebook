'use strict';

var navbarDrct = angular.module('yachtGuidebook.navbar');

navbarDrct.directive('ygbNavbar', function () {
    return {
        restrict: 'AE',
        templateUrl: 'static/angular_app/navbar/navbar.html',
        controller: ['$scope', 'FiltersObjectSrv', 'CurrentUserSrv', 'TotalInComparisonSrv', '$state', 'StoreLastStateSrv', function ($scope, FiltersObjectSrv, CurrentUserSrv, TotalInComparisonSrv, $state, StoreLastStateSrv) {
            $scope.currentUser = CurrentUserSrv.profile;
            $scope.lastParams = StoreLastStateSrv.getState();
            $scope.searchText = '';

            $scope.userLogout = function () {
                CurrentUserSrv.removeProfile();
                $state.reload();
            };

            $scope.$watch('currentUser.isLoggedIn', function(newValue){
                if (newValue) {
                    TotalInComparisonSrv.total.reloadCount();
                }
            });

            $scope.total = TotalInComparisonSrv.total;


            $scope.applySearch = function () {
                if ($scope.searchText) {
                    $state.go('yachtSearch.yachtList', {search: $scope.searchText, page: null}).then(function(){
                        FiltersObjectSrv.initSearching();
                        $scope.searchText = '';
                    });
                }
            };

        }]
    };
});
