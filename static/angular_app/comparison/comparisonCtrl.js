'use strict';


var yachtComparison = angular.module('yachtGuidebook.yachtComparison');


yachtComparison.controller('YachtComparisonCtrl', ['$scope', '$state', 'YachtComparisonSrv', 'TotalInComparisonSrv', 'compareYachtList', 'ErrorAlertSrv', function ($scope, $state, YachtComparisonSrv, TotalInComparisonSrv, compareYachtList, ErrorAlertSrv) {
    if (compareYachtList.length) {
        $scope.yachts = compareYachtList;
    } else {
        $state.go('yachtSearch.yachtList');
    }

    $scope.removeFromComparison = function (yachtSlug) {
        YachtComparisonSrv.delete(
            {yachtSlug: yachtSlug},
            function success(response) {
                TotalInComparisonSrv.total.reloadCount();
                $state.reload();
            },
            function error() {
                ErrorAlertSrv.showAlert();
            }
        );
    };
}]);
