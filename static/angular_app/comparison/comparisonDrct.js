'use strict';

var yachtComparison = angular.module('yachtGuidebook.yachtComparison');


yachtComparison.directive('ygbAddToComparisonBtn', [function () {
    return {
        restrict: 'AE',
        templateUrl: 'static/angular_app/comparison/add-btn.html',
        scope: {
            yachtSlug: '=',
            btnClass: '@'
        },
        controller: ['$scope', 'YachtComparisonSrv', 'CurrentUserSrv', 'TotalInComparisonSrv', 'ErrorAlertSrv', function ($scope, YachtComparisonSrv, CurrentUserSrv, TotalInComparisonSrv, ErrorAlertSrv) {
            $scope.currentUser = CurrentUserSrv.profile;

            var checkYachtInComparisonList = function () {
                YachtComparisonSrv.exists(
                    {yachtSlug: $scope.yachtSlug},
                    function success(response) {
                        if (response.exists) {
                            $scope.isYachtInComparisonList = true;
                            $scope.tooltipText = 'Remove yacht from comparison list'
                        } else {
                            $scope.isYachtInComparisonList = false;
                            $scope.tooltipText = 'Add yacht to comparison list'
                        }
                    }
                );
            };

            $scope.addToComparisonList = function () {
                if ($scope.isYachtInComparisonList) {
                    YachtComparisonSrv.delete(
                        {yachtSlug: $scope.yachtSlug},
                        function success(response) {
                            TotalInComparisonSrv.total.reloadCount();
                            checkYachtInComparisonList();
                        },
                        function error() {
                            ErrorAlertSrv.showAlert();
                        }
                    );
                } else {
                    YachtComparisonSrv.add(
                        {yacht: $scope.yachtSlug},
                        function success(response) {
                            TotalInComparisonSrv.total.reloadCount();
                            checkYachtInComparisonList();
                        },
                        function error() {
                            ErrorAlertSrv.showAlert();
                        }
                    );
                }
            };

            if ($scope.currentUser.isLoggedIn) {
                checkYachtInComparisonList();
            }
        }]
    }
}]);
