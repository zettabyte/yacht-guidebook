'use strict';

var comparison = angular.module('yachtGuidebook.yachtComparison');

comparison.factory('YachtComparisonSrv', ['$resource', function ($resource) {
    return $resource('api/compare-yachts/:yachtSlug', {}, {
        'exists': {method: 'GET', url:'api/compare-yachts/:yachtSlug/exists'},
        'query': {method: 'GET', isArray: true},
        'count': {method: 'GET', url: 'api/compare-yachts/total-count'},
        'add': {method: 'POST'},
        'delete': {method: 'DELETE'}
    });
}]);


comparison.factory('TotalInComparisonSrv', ['YachtComparisonSrv', function (YachtComparisonSrv) {
    var total = {
        count: null,
        reloadCount: function () {
            YachtComparisonSrv.count({},
                function success(response) {
                    total.count = response.count;
                }
            );
        }
    };

    return {
        total: total
    }
}]);