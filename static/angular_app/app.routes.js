'use strict';


var appRoutes = angular.module('appRoutes', []);


appRoutes.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider
        .state('home', {
            url: '/',
            templateUrl: 'static/angular_app/flatpages/home.html',
            onEnter: ['HeadMetaSrv', function (HeadMetaSrv) {
                HeadMetaSrv.meta.setTitle('');
            }]
        })
        .state('registration', {
            url: '/registration',
            templateUrl: 'static/angular_app/user-account/registration.html',
            onEnter: ['HeadMetaSrv', '$state', 'CurrentUserSrv', function (HeadMetaSrv, $state, CurrentUserSrv) {
                if (CurrentUserSrv.profile.isLoggedIn) {
                    $state.go('home');
                }
                HeadMetaSrv.meta.setTitle('Registration');
            }]
        })
        .state('profile', {
            url: '/profile',
            templateUrl: 'static/angular_app/user-account/profile.html',
            controller: 'ProfileCtrl',
            resolve: {
                userProfile: ['CurrentUserSrv', 'UserProfileSrv', function (CurrentUserSrv, UserProfileSrv) {
                    if (CurrentUserSrv.profile.isLoggedIn) {
                        return UserProfileSrv.get({currentUsername: CurrentUserSrv.profile.username}).$promise;
                    }
                }]
            },
            onEnter: ['HeadMetaSrv', '$state', 'CurrentUserSrv', function (HeadMetaSrv, $state, CurrentUserSrv) {
                if (!CurrentUserSrv.profile.isLoggedIn) {
                    $state.go('login');
                }
                HeadMetaSrv.meta.setTitle('Profile');
            }]
        })
        .state('login', {
            url: '/login',
            templateUrl: 'static/angular_app/user-account/login.html',
            onEnter: ['HeadMetaSrv', '$state', 'CurrentUserSrv', function (HeadMetaSrv, $state, CurrentUserSrv) {
                if (CurrentUserSrv.profile.isLoggedIn) {
                    $state.go('home');
                }
                HeadMetaSrv.meta.setTitle('Login');
            }]
        })
        .state('yachtSearch', {
            //url:'',
            views: {
                '': {templateUrl: 'static/angular_app/yacht-search/yacht-search.html'},
                'filterList@yachtSearch': {
                    templateUrl: 'static/angular_app/yacht-search/filter-list.html',
                    controller: 'FilterListCtrl'
                },
                'userFilters@yachtSearch': {templateUrl: 'static/angular_app/yacht-search/user-filters.html'},
                'activeFilters@yachtSearch': {templateUrl: 'static/angular_app/yacht-search/active-filters.html'},
                'orderBy@yachtSearch': {templateUrl: 'static/angular_app/yacht-search/order-by.html'}
            },
            resolve: {
                filterOptions: ['FiltersOptionsSrv', function (FiltersOptionsSrv) {
                    return FiltersOptionsSrv.query().$promise;
                }]
            },
            onEnter: ['HeadMetaSrv', function (HeadMetaSrv) {
                HeadMetaSrv.meta.setTitle('Yacht models ');
            }]
        })
        .state('yachtSearch.yachtList', {
            url: '/yachts?search&ordering&page&brand&type&cabins&length&draft',
            params: {
                brand: {array: true},
                type: {array: true},
                cabins: {array: true},
                length: {array: true},
                draft: {array: true}
            },
            templateUrl: 'static/angular_app/yacht-search/yacht-list.html',
            controller: 'YachtListCtrl',
            resolve: {
                yachtList: ['YachtListSrv', '$stateParams', function (YachtListSrv, $stateParams) {
                    return YachtListSrv.query($stateParams).$promise;
                }]
            }
        })
        .state('yachtDetail', {
            url: '/yachts/:yachtSlug',
            views: {
                '': {
                    templateUrl: 'static/angular_app/yacht-detail/yacht-detail.html',
                    controller: 'YachtDetailCtrl'
                },
                'yachtRating@yachtDetail': {templateUrl: 'static/angular_app/yacht-detail/yacht-rating.html'},
                'comments@yachtDetail': {templateUrl: 'static/angular_app/yacht-detail/comments.html'}
            },
            resolve: {
                yachtDetails: ['YachtDetailSrv', '$stateParams', function (YachtDetailSrv, $stateParams) {
                    return YachtDetailSrv.get($stateParams).$promise;
                }]
            },
            onEnter: ['yachtDetails', 'HeadMetaSrv', function (yachtDetails, HeadMetaSrv) {
                HeadMetaSrv.meta.setTitle(yachtDetails.title);
                HeadMetaSrv.meta.setDescription('Specification of ' + yachtDetails.title + '. Length: ' + yachtDetails.length_over_all + ' ft, beam: ' + yachtDetails.beam + ' ft, draft: ' + yachtDetails.draft + ' ft');

            }]
        })
        .state('comparison', {
            url: '/comparison',
            templateUrl: 'static/angular_app/comparison/comparison.html',
            controller: 'YachtComparisonCtrl',
            resolve: {
                compareYachtList: ['YachtComparisonSrv', function (YachtComparisonSrv) {
                    return YachtComparisonSrv.query().$promise;
                }]
            },
            onEnter: ['HeadMetaSrv', function (HeadMetaSrv) {
                HeadMetaSrv.meta.setTitle('Compare yacht models');
            }]
        })
        .state('404', {
            templateUrl: 'static/angular_app/flatpages/404.html',
            onEnter: ['HeadMetaSrv', function (HeadMetaSrv) {
                HeadMetaSrv.meta.setTitle('404');
            }]
        });

    $urlRouterProvider.otherwise(function($injector){
        $injector.invoke(['$state', function($state) {
            $state.go('404', {}, { location: false });
        }]);
    });

    $locationProvider.html5Mode(true);
}]);