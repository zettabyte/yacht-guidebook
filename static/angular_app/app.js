'use strict';


angular.module('yachtGuidebook', [
    'ui.router',
    'ui.bootstrap',
    'ngAnimate',
    'ngResource',

    'appRoutes',
    'appConfig',
    'appRun',

    'yachtGuidebook.common',
    'yachtGuidebook.navbar',
    'yachtGuidebook.footer',
    'yachtGuidebook.userAccount',
    'yachtGuidebook.yachtSearch',
    'yachtGuidebook.yachtDetail',
    'yachtGuidebook.yachtComparison',
    'yachtGuidebook.headMeta'
]);


angular.module('yachtGuidebook.common', []);
angular.module('yachtGuidebook.navbar', []);
angular.module('yachtGuidebook.footer', []);
angular.module('yachtGuidebook.userAccount', []);
angular.module('yachtGuidebook.yachtSearch', []);
angular.module('yachtGuidebook.yachtDetail', []);
angular.module('yachtGuidebook.yachtComparison', []);
angular.module('yachtGuidebook.headMeta', []);

