'use strict';


var commonSrv = angular.module('yachtGuidebook.common');


commonSrv.factory('LocalStorageSrv', ['$window', function ($window) {
    var store = $window.localStorage;

    var add = function (key, value) {
        value = angular.toJson(value);
        store.setItem(key, value);
    };

    var get = function (key) {
        var value = store.getItem(key);
        if (value) {
            value = angular.fromJson(value);
        }
        return value;
    };

    var remove = function (key) {
        store.removeItem(key);
    };

    return {
        add: add,
        get: get,
        remove: remove
    };
}]);


commonSrv.factory('ErrorAlertSrv', [function () {
    return {
        showAlert: function () {
            alert('Operation cannot be performed! Unknown error has occurred. We are sorry.\nPlease try to reload page or contact website administrator.')
        }
    }
}]);


commonSrv.factory('StoreLastStateSrv', ['$state', function ($state) {
    var lastState = {};

    return {
        setState: function (objName, state, params) {
            lastState[objName] = {state: state, params: params};
        },
        getState: function () {
            return lastState;
        }
    }
}]);