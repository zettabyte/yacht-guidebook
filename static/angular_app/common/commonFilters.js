'use strict';


var commonFilters = angular.module('yachtGuidebook.common');


commonFilters.filter('gal2l', function(){
    return function(input) {
        return input * 3.78541
    };
});


commonFilters.filter('ft2m', function(){
    return function(input) {
        return input * 0.3048
    };
});


commonFilters.filter('lbs2kg', function(){
    return function(input) {
        return input * 0.453592
    };
});


commonFilters.filter('amount', function(){
    return function(input) {
        return input? input + ' x ': ''
    };
});
