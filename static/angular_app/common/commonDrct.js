'use strict';


var commonDrct = angular.module('yachtGuidebook.common');


commonDrct.directive('ygbFloatingLabels', [function(){
    return {
        restrict: 'A',
        scope: {},
        link: function(scope, el, attrs){
            el.find('input').on('focus blur change', function(e){
                el.toggleClass('focused', (e.type === 'focus' || el.find('input').val().length > 0));
            });
        }
    }
}]);


commonDrct.directive('ygbPhotoGallery', [function(){
    return {
        restrict: 'A',
        link: function(scope, el, attrs) {
            el.magnificPopup({delegate: 'a.ygb-popup-img', type:'image', gallery: {enabled:true}, image:{titleSrc: 'photo-title'}});
        }
    }
}]);


commonDrct.directive('ygbSpecValue', ['$compile', function($compile){
    return {
        restrict: 'E',
        scope: {
            uscsValue: '=',
            uscsMeasure: '@',
            amount: '='
        },
        link: function(scope, el, attrs){
            if (scope.uscsValue) {
                var innerHtml = '{{ amount | amount}}{{ uscsValue | number: 1 }} {{ uscsMeasure }} <span class="text-muted">({{ amount | amount }}{{ uscsValue | {filtername} | number: 2 }} {{ msMeasure }})</span>';
                switch (scope.uscsMeasure) {
                    case 'ft':
                        innerHtml = innerHtml.replace('{filtername}', 'ft2m');
                        scope.msMeasure = 'm';
                        break;
                    case 'lbs':
                        innerHtml = innerHtml.replace('{filtername}', 'lbs2kg');
                        scope.msMeasure = 'kg';
                        break;
                    case 'gal':
                        innerHtml = innerHtml.replace('{filtername}', 'gal2l');
                        scope.msMeasure = 'l';
                        break;
                    case 'gal/h':
                        innerHtml = innerHtml.replace('{filtername}', 'gal2l');
                        scope.msMeasure = 'l/h';
                        break;
                }
                el.html(innerHtml);
                $compile(el.contents())(scope);
            }
        }
    }
}]);