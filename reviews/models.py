from django.db import models
from django.conf import settings
from django.core import validators

from yachts.models import Yacht


class Comment(models.Model):
    yacht = models.ForeignKey(Yacht)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    text = models.TextField()
    date_create = models.DateTimeField(auto_now_add=True, null=True)
    date_update = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.yacht.__str__() + ' | ' + self.user.__str__()

    class Meta:
        ordering = ('-date_create',)


class Rating(models.Model):
    yacht = models.ForeignKey(Yacht)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    score = models.SmallIntegerField(validators=[validators.MinValueValidator(1), validators.MaxValueValidator(5)])
    date_create = models.DateTimeField(auto_now_add=True, null=True)
    date_update = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.yacht.__str__() + ' | ' + self.user.__str__() + ' | ' + str(self.score)

    class Meta:
        unique_together = ('yacht','user')