from rest_framework.test import APITestCase
from rest_framework import status
from rest_framework.authtoken.models import Token

import yachts.tests as test_yachts
import user_profiles.tests as test_user

from .models import Comment, Rating


class CommentTests(APITestCase):
    def setUp(self):
        test_yachts.create_test_brand('Bavaria')
        test_yachts.create_test_type('Sailing yacht')
        self.yacht_1 = test_yachts.create_test_yacht('Bavaria 31 Cruiser', published=True)
        self.yacht_2 = test_yachts.create_test_yacht('Bavaria 34 Cruiser', published=True)
        self.user = test_user.create_test_user()

    def test_add_patch_delete_comment_unauthorized_client(self):
        response = self.client.post('/api/comments', data={'yacht': self.yacht_1.slug, 'text': 'New comment'})
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        response = self.client.patch('/api/comments/1', data={'text': 'Comment update!'})
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        response = self.client.delete('/api/comments/1')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_add_comment_authorized_client(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + Token.objects.get(user=self.user).key)
        response = self.client.post('/api/comments', data={'yacht': self.yacht_1.slug})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        response = self.client.post('/api/comments', data={'text': 'New comment'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        response = self.client.post('/api/comments', data={'yacht': self.yacht_1.slug, 'text': 'New comment'})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_delete_comment_authorized_client(self):
        comment = Comment.objects.create(yacht=self.yacht_1, text='New comment!', user=self.user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + Token.objects.get(user=self.user).key)
        response = self.client.delete('/api/comments/' + str(comment.id))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_update_comment_authorized_client(self):
        comment = Comment.objects.create(yacht=self.yacht_1, text='Some comment!', user=self.user)
        new_text = 'Comment was updated!'
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + Token.objects.get(user=self.user).key)
        self.client.patch('/api/comments/' + str(comment.id), data={'text': new_text})
        self.assertEqual(Comment.objects.get(id=comment.id).text, new_text)

    def test_get_comments(self):
        Comment.objects.create(yacht=self.yacht_1, text='Some comment1', user=self.user)
        Comment.objects.create(yacht=self.yacht_1, text='New comment2', user=self.user)
        Comment.objects.create(yacht=self.yacht_2, text='Some comment', user=self.user)
        response = self.client.get('/api/comments')
        self.assertTrue(len(response.data) == 3)
        response = self.client.get('/api/comments?yacht__slug=' + self.yacht_1.slug)
        self.assertTrue(len(response.data) == 2)
        response = self.client.get('/api/comments?yacht__slug=' + self.yacht_2.slug)
        self.assertTrue(len(response.data) == 1)
        response = self.client.get('/api/comments?yacht__slug=fake-yacht')
        self.assertTrue(len(response.data) == 0)


class RatingTests(APITestCase):
    def setUp(self):
        test_yachts.create_test_brand('Bavaria')
        test_yachts.create_test_type('Sailing yacht')
        self.yacht_1 = test_yachts.create_test_yacht('Bavaria 31 Cruiser', published=True)
        self.yacht_2 = test_yachts.create_test_yacht('Bavaria 34 Cruiser', published=True)
        self.user_1 = test_user.create_test_user('user_1')
        self.user_2 = test_user.create_test_user('user_2')

    def test_create_update_get_rating_unauthorized_client(self):
        response = self.client.post('/api/rating', data={'yacht': self.yacht_1.slug, 'score': 4})
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        response = self.client.patch('/api/rating/'+self.yacht_1.slug, data={'score': 5})
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        response = self.client.get('/api/rating/'+self.yacht_1.slug)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_rating_authorized_client(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + Token.objects.get(user=self.user_1).key)
        response = self.client.post('/api/rating', data={'score': 4})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        response = self.client.post('/api/rating', data={'yacht': self.yacht_1.slug})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        response = self.client.post('/api/rating', data={'yacht': self.yacht_1.slug, 'score': 4})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update_rating_authorized_client(self):
        Rating.objects.create(user=self.user_1, yacht=self.yacht_1, score=4)
        new_score = 5
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + Token.objects.get(user=self.user_1).key)
        self.client.patch('/api/rating/'+self.yacht_1.slug, data={'score': new_score})
        self.assertEqual(Rating.objects.get().score, new_score)

    def test_get_avg_score(self):
        Rating.objects.create(user=self.user_1, yacht=self.yacht_1, score=4)
        Rating.objects.create(user=self.user_2, yacht=self.yacht_1, score=2)
        response = self.client.get('/api/rating/'+self.yacht_1.slug + '/avg_score')
        self.assertEqual(response.data['total_votes'], 2)
        self.assertEqual(response.data['avg_score'], 3)
