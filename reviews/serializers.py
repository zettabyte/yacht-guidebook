from rest_framework import serializers

from .models import Comment, Rating
from yachts.models import Yacht



class CommentSerializer(serializers.ModelSerializer):
    user = serializers.SlugRelatedField(read_only=True, slug_field='username', default=serializers.CurrentUserDefault())
    yacht = serializers.SlugRelatedField(slug_field='slug', queryset=Yacht.objects.all())

    class Meta:
        model = Comment
        fields = ('id', 'text', 'user', 'yacht', 'date_create')


class RatingSerializer(serializers.ModelSerializer):
    user = serializers.SlugRelatedField(read_only=True, slug_field='username', default=serializers.CurrentUserDefault())
    yacht = serializers.SlugRelatedField(slug_field='slug', queryset=Yacht.objects.all())

    class Meta:
        model = Rating
        fields = ('id', 'user', 'yacht', 'score')
