from django.db.models import Avg, Count
from django.shortcuts import get_object_or_404

from rest_framework.decorators import detail_route
from rest_framework.response import Response
from rest_framework import viewsets, mixins
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework import filters

from .models import Comment, Rating
from .serializers import CommentSerializer, RatingSerializer
from .permissions import IsOwnerOrReadOnly


class CommentViewSet(viewsets.ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('yacht__slug',)
    permission_classes = (IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly)


class RatingViewSet(mixins.CreateModelMixin, mixins.RetrieveModelMixin, mixins.UpdateModelMixin, viewsets.GenericViewSet):
    queryset = Rating.objects.all()
    serializer_class = RatingSerializer
    permission_classes = (IsAuthenticated, IsOwnerOrReadOnly,)
    lookup_field = 'yacht__slug'

    def get_object(self):
        obj = get_object_or_404(self.get_queryset(), user=self.request.user.id, yacht__slug=self.kwargs['yacht__slug'])
        return obj

    @detail_route(methods=['get'], permission_classes=[AllowAny])
    def avg_score(self, request, pk=None, *args, **kwargs):
        queryset = self.get_queryset().filter(yacht__slug=kwargs.get('yacht__slug', '')).aggregate(avg_score=Avg('score'), total_votes=Count('user'))
        return Response(queryset)