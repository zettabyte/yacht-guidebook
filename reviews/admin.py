from django.contrib import admin

from .models import Comment, Rating



class CommentAdmin(admin.ModelAdmin):
    list_display = ('yacht', 'user', 'date_create', 'date_update')


class RatingAdmin(admin.ModelAdmin):
    list_display = ('yacht', 'user', 'score')



admin.site.register(Comment, CommentAdmin)
admin.site.register(Rating, RatingAdmin)