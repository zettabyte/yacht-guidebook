from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic.base import TemplateView
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework.routers import DefaultRouter
from yachts.views import YachtViewSet
from reviews.views import CommentViewSet, RatingViewSet
from user_profiles.views import UserViewSet
from yacht_filtering.views import UserStoredFiltersViewSet, FilteringOptionsViewSet
from yacht_comparison.views import ComparableYachtListViewSet


router = DefaultRouter(trailing_slash=False)
router.register(r'yachts', YachtViewSet)
router.register(r'filters/user-filters', UserStoredFiltersViewSet)
router.register(r'filters/options', FilteringOptionsViewSet, base_name='FilteringOptions')
router.register(r'compare-yachts', ComparableYachtListViewSet)
router.register(r'comments', CommentViewSet)
router.register(r'rating', RatingViewSet)
router.register(r'users', UserViewSet)


urlpatterns = static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api-token-auth$', obtain_auth_token),
    url(r'^robots\.txt$', TemplateView.as_view(template_name='robots.txt', content_type='text/plain')),
    url(r'^.*$', TemplateView.as_view(template_name = 'base.html')),
]

