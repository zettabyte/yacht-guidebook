from django.db import models
from django.conf import settings

from yachts.models import Yacht



class ComparableYachtList(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    yacht = models.ForeignKey(Yacht)

    def __str__(self):
        return self.user.username + ' | ' + self.yacht.title

    class Meta:
        unique_together = ('user', 'yacht')