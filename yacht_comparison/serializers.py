from rest_framework import serializers
from .models import ComparableYachtList
from yachts.models import Yacht
from yachts.serializers import BrandSerializer, AccommodationSerializer, EngineSerializer

class ComparableYachtsSerializer(serializers.ModelSerializer):
    brand = BrandSerializer(read_only=True)
    type = serializers.SlugRelatedField(slug_field='title', read_only=True)
    accommodation_set = AccommodationSerializer(many=True, read_only=True)
    engine_set = EngineSerializer(many=True, read_only=True)

    class Meta:
        model = Yacht
        fields = ('title', 'card_img', 'slug', 'brand', 'type', 'length_over_all', 'beam', 'draft', 'weight_dry',
                  'accommodation_set', 'engine_set', 'amount_of_fuel_tanks', 'fuel_tank', 'amount_of_water_tanks',
                  'water_tank', 'website')


class ComparableYachtListSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    yacht = serializers.SlugRelatedField(slug_field='slug', queryset=Yacht.objects.all())

    class Meta:
        model = ComparableYachtList
        fields = ('user', 'yacht')
