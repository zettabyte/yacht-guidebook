from django.shortcuts import get_object_or_404

from rest_framework import mixins, viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import list_route, detail_route

from .serializers import ComparableYachtListSerializer, ComparableYachtsSerializer
from .models import ComparableYachtList
from yachts.models import Yacht



class ComparableYachtListViewSet(mixins.CreateModelMixin, mixins.ListModelMixin, mixins.DestroyModelMixin, viewsets.GenericViewSet):
    queryset = ComparableYachtList.objects.all()
    serializer_class = ComparableYachtListSerializer
    lookup_field = 'yacht__slug'
    permission_classes = (IsAuthenticated, )

    def get_object(self):
        return get_object_or_404(self.get_queryset(), user=self.request.user.id, yacht__slug=self.kwargs['yacht__slug'])

    def get_queryset(self):
        return ComparableYachtList.objects.filter(user=self.request.user.id).all()

    def list(self, request, *args, **kwargs):
        queryset = Yacht.objects.published().filter(id__in=self.get_queryset().values_list('yacht', flat=True))
        serializer = ComparableYachtsSerializer(queryset, many=True)
        return Response(serializer.data)

    @list_route(methods=['GET'], url_path='total-count')
    def total_count(self, request, *args, **kwargs):
        return Response({'count': self.get_queryset().filter(yacht__published=True).count()})

    @detail_route(methods=['GET'], url_path='exists')
    def exists(self, request, pk=None, *args, **kwargs):
        return Response({'exists': self.get_queryset().filter(user=self.request.user.id, yacht__slug=self.kwargs['yacht__slug'], yacht__published=True).exists()})
