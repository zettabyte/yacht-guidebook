from django.apps import AppConfig


class YachtComparisonConfig(AppConfig):
    name = 'yacht_comparison'
