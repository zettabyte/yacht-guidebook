from rest_framework.test import APITestCase
from rest_framework import status
from rest_framework.authtoken.models import Token

import yachts.tests as test_yachts
import user_profiles.tests as test_user

from .models import ComparableYachtList


class ComparableYachtListTests(APITestCase):
    def setUp(self):
        test_yachts.create_test_brand('Bavaria')
        test_yachts.create_test_type('Sailing yacht')
        self.yacht_1 = test_yachts.create_test_yacht('Bavaria 31 Cruiser', published=True)
        self.yacht_2 = test_yachts.create_test_yacht('Bavaria 34 Cruiser', published=True)
        self.user = test_user.create_test_user()

    def test_add_yacht_unauthorized_client(self):
        response = self.client.post('/api/compare-yachts', data={'yacht': self.yacht_1.slug})
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_add_yacht_authorized_client(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + Token.objects.get(user=self.user).key)
        response = self.client.post('/api/compare-yachts', data={'yacht': self.yacht_1.slug})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_add_fake_yacht_authorized_client(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + Token.objects.get(user=self.user).key)
        response = self.client.post('/api/compare-yachts', data={'yacht': 'fake-yacht'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_delete_yacht_unauthorized_client(self):
        response = self.client.delete('/api/compare-yachts', data={'yacht': self.yacht_1.slug})
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_delete_yacht_authorized_client(self):
        ComparableYachtList.objects.create(user=self.user, yacht=self.yacht_1)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + Token.objects.get(user=self.user).key)
        response = self.client.delete('/api/compare-yachts/' + self.yacht_1.slug)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_fake_yacht_authorized_client(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + Token.objects.get(user=self.user).key)
        response = self.client.delete('/api/compare-yachts/fake-yacht')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_yacht_list_authorized_client(self):
        ComparableYachtList.objects.create(user=self.user, yacht=self.yacht_1)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + Token.objects.get(user=self.user).key)
        response = self.client.get('/api/compare-yachts')
        self.assertEqual(response.data[0]['slug'], self.yacht_1.slug)
        ComparableYachtList.objects.create(user=self.user, yacht=self.yacht_2)
        response = self.client.get('/api/compare-yachts')
        self.assertTrue(len(response.data) == 2)

    def test_get_total_count_unauthorized_client(self):
        response = self.client.get('/api/compare-yachts/total-count')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_total_count_authorized_client(self):
        ComparableYachtList.objects.create(user=self.user, yacht=self.yacht_1)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + Token.objects.get(user=self.user).key)
        response = self.client.get('/api/compare-yachts/total-count')
        self.assertEqual(response.data['count'], 1)
        ComparableYachtList.objects.create(user=self.user, yacht=self.yacht_2)
        response = self.client.get('/api/compare-yachts/total-count')
        self.assertEqual(response.data['count'], 2)

    def test_get_exists_unauthorized_client(self):
        response = self.client.get('/api/compare-yachts/' + self.yacht_1.slug + '/exists')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_exists_authorized_client(self):
        ComparableYachtList.objects.create(user=self.user, yacht=self.yacht_1)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + Token.objects.get(user=self.user).key)
        response = self.client.get('/api/compare-yachts/' + self.yacht_1.slug + '/exists')
        self.assertTrue(response.data['exists'])
        response = self.client.get('/api/compare-yachts/' + self.yacht_2.slug + '/exists')
        self.assertFalse(response.data['exists'])
