from rest_framework import serializers
from .models import UserStoredFilters, RangeFilterOptions
from yachts.models import Type, Brand

class UserStoredFiltersSerializer(serializers.ModelSerializer):
    user = serializers.SlugRelatedField(read_only=True, slug_field='username', default=serializers.CurrentUserDefault())

    class Meta:
        model = UserStoredFilters
        fields = ('id', 'user', 'title', 'json')


class RangeFilterOptionsSerializer(serializers.ModelSerializer):

    class Meta:
        model = RangeFilterOptions
        fields = ('title', 'slug')


class TypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Type
        fields = ('title', 'slug')


class BrandSerializer(serializers.ModelSerializer):

    class Meta:
        model = Brand
        fields = ('title', 'slug')
