from django.db import models
from django.conf import settings



class UserStoredFilters(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    title = models.CharField(max_length=250)
    json = models.TextField()

    def __str__(self):
        return self.user.username + ' | ' + self.title


class RangeFilterOptions(models.Model):
    RANGE_FILTERS = (('length', 'Length'), ('draft', 'Draft'))

    filter = models.CharField(max_length=250, choices=RANGE_FILTERS)
    title = models.CharField(max_length=250)
    slug = models.CharField(max_length=250)
    min_value = models.FloatField(null=True, blank=True)
    max_value = models.FloatField(null=True, blank=True)
    order_id = models.SmallIntegerField(null=True, blank=True)

    def __str__(self):
        return self.filter + ' | ' + self.title

    class Meta:
        ordering = ('filter', 'order_id')