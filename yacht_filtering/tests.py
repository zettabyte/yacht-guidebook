from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token
from rest_framework import status

import yachts.tests as test_yachts
import user_profiles.tests as user_tests
from .models import RangeFilterOptions, UserStoredFilters


def create_test_range_filters(filter, min=None, max=None):
    title = str(min) + ' - ' + str(max) if min & max else min or max
    slug = str(min) + '-' + str(max) if min & max else min or max
    return RangeFilterOptions.objects.create(filter=filter, title=title, slug=slug)


class RangeFiltersTests(APITestCase):
    def setUp(self):
        test_yachts.create_test_brand('Bavaria')
        test_yachts.create_test_type('Sailing yacht')
        yacht = test_yachts.create_test_yacht('Bavaria 34 Cruiser', published=True, length=34, draft=6.5)
        test_yachts.create_test_accommodation(yacht=yacht, cabins=2)
        create_test_range_filters('length', 25, 35)
        create_test_range_filters('draft', 6, 7)

    def test_filtering_options_list(self):
        response = self.client.get('/api/filters/options')
        self.assertEqual(response.data['filters']['length'][0]['slug'], '25-35')
        self.assertEqual(response.data['filters']['draft'][0]['slug'], '6-7')
        self.assertEqual(response.data['filters']['brand'][0]['slug'], 'bavaria')
        self.assertEqual(response.data['filters']['type'][0]['slug'], 'sailing-yacht')
        self.assertEqual(response.data['filters']['cabins'][0]['slug'], '2')
        self.assertTrue(len(response.data['ordering']) > 0)

    def test_filter_yacht_list_by_range_filters(self):
        response = self.client.get('/api/yachts?length=25-35&draft=6-7')
        self.assertEqual(response.data['results'][0]['slug'], 'bavaria-34-cruiser')
        response = self.client.get('/api/yachts?length==fake-length-slug')
        self.assertTrue(len(response.data['results']) == 0)
        response = self.client.get('/api/yachts?draft==fake-draft-slug')
        self.assertTrue(len(response.data['results']) == 0)


class UserStoredFiltersTests(APITestCase):
    def setUp(self):
        self.user = user_tests.create_test_user('user0'), user_tests.create_test_user('user1')
        self.user0_filter = UserStoredFilters.objects.create(user=self.user[0], title='User0 filter', json='{"key": "value"}')
        self.user1_filter = UserStoredFilters.objects.create(user=self.user[1], title='User1 filter', json='{"key": "value"}')

    def test_get_filter_list(self):
        response = self.client.get('/api/filters/user-filters')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + Token.objects.get(user=self.user[0]).key)
        response = self.client.get('/api/filters/user-filters')
        self.assertEqual(response.data[0]['title'], 'User0 filter')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + Token.objects.get(user=self.user[1]).key)
        response = self.client.get('/api/filters/user-filters')
        self.assertEqual(response.data[0]['title'], 'User1 filter')

    def test_save_filter(self):
        response = self.client.post('/api/filters/user-filters')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + Token.objects.get(user=self.user[0]).key)
        response = self.client.post('/api/filters/user-filters', data={'title': "New filter", 'json': '{"key":"value"}'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(len(UserStoredFilters.objects.filter(user=self.user[0])) == 2)

    def test_delete_filter(self):
        response = self.client.delete('/api/filters/user-filters/1')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + Token.objects.get(user=self.user[0]).key)
        response = self.client.delete('/api/filters/user-filters/'+str(self.user1_filter.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        response = self.client.delete('/api/filters/user-filters/'+str(self.user0_filter.id))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
