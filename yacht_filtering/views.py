from rest_framework import mixins, viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .serializers import UserStoredFiltersSerializer, RangeFilterOptionsSerializer, BrandSerializer, TypeSerializer
from .models import UserStoredFilters, RangeFilterOptions

from yachts.models import Brand, Type, Accommodation, Yacht


class UserStoredFiltersViewSet(mixins.CreateModelMixin, mixins.ListModelMixin, mixins.DestroyModelMixin, viewsets.GenericViewSet):
    queryset = UserStoredFilters.objects.all()
    serializer_class = UserStoredFiltersSerializer
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
            return self.queryset.filter(user=self.request.user.id).all()


class FilteringOptionsViewSet(viewsets.ViewSet):
    ORDERING = [{'slug': 'length_over_all', 'title': 'Length (ascending)'},
                {'slug': '-length_over_all', 'title': 'Length (descending)'}]

    def list(self, request):
        published_yacht_list = Yacht.objects.published()
        return Response({'filters': {'brand': BrandSerializer(Brand.objects.filter(id__in=published_yacht_list.values('brand')).all(), many=True).data,
                                     'type': TypeSerializer(Type.objects.filter(id__in=published_yacht_list.values('type')).all(), many=True).data,
                                     'cabins': list(map(lambda i: {'title': str(i), 'slug': str(i)}, Accommodation.objects.filter(yacht__in=published_yacht_list).values_list('cabins', flat=True).distinct().order_by('cabins'))),
                                     'length': RangeFilterOptionsSerializer(RangeFilterOptions.objects.filter(filter='length'), many=True).data,
                                     'draft': RangeFilterOptionsSerializer(RangeFilterOptions.objects.filter(filter='draft'), many=True).data},
                         'ordering': FilteringOptionsViewSet.ORDERING})