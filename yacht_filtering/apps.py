from django.apps import AppConfig


class YachtFilteringConfig(AppConfig):
    name = 'yacht_filtering'
