from django.contrib import admin
from .models import UserStoredFilters, RangeFilterOptions


class RangeFilterOptionsAdmin(admin.ModelAdmin):
    list_filter = ('filter',)
    list_display = ('title', 'filter', 'order_id')
    list_editable = ('order_id',)


admin.site.register(UserStoredFilters)
admin.site.register(RangeFilterOptions, RangeFilterOptionsAdmin)
